//
//  ECSSFlipsideViewController.m
//  Elizabethtown College Student Senate
//
//  Created by Matt Shank on 6/17/13.
//  Copyright (c) 2013 Student Senate. All rights reserved.
//

#import "ECSSFlipsideViewController.h"

@interface ECSSFlipsideViewController ()

@end

@implementation ECSSFlipsideViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)done:(id)sender
{
    [self.delegate flipsideViewControllerDidFinish:self];
}

@end
